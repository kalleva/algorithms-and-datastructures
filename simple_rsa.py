#!/usr/bin/env python

import random
from enhanced_primality_with_carmichael_numbers import is_prime
from extended_euclid import extended_euclid
from modular_exponentiation import mod_pow


def rsa(n):
    """n - number of bits;
    return public key e, N and private key"""
    p, q = rand_prime_pair(n)  # pick two n-bit random primes
    num = p * q  # number modulo which we will work
    e, private_key = exponent_and_private_key(p, q)
    return e, p * q, private_key


def rand_prime_pair(n):
    """generates pair of random primes length n bits"""
    lb = 2 ** n
    hb = 2 ** (n + 1) - 1
    p = random.randint(lb, hb)
    while not is_prime(p):
        p = random.randint(lb, hb)
    q = random.randint(lb, hb)
    while not is_prime(q) and q != p:
        q = random.randint(lb, hb)
    return p, q


def exponent_and_private_key(p, q):
    """generates e relative prime with (p -1 ) * (q - 1)
    which will be exponent and private key"""
    l = [3, 5, 7, 11, 13, 17, 19]  # arbitrary possible exponents
    n = (p - 1) * (q - 1)
    for i in l:
        x, y, d = extended_euclid(i, n)
        if d == 1:
            return i, x % n


def encrypt(msg, e, n):
    """encrypts message with public key e, n"""
    return mod_pow(msg, e, n)


# decrypt is for convenience
# it's possible to encrypt and decrypt with one function


def decrypt(msg, private_key, n):
    """decrypts message with private key"""
    return mod_pow(msg, private_key, n)


def main():
    for i in range(10):
        e, n, pr_key = rsa(10)
        assert 13 == decrypt(encrypt(13, e, n), pr_key, n)

    e, n, pr_key = rsa(25)
    assert 1356789 == decrypt(encrypt(1356789, e, n), pr_key, n)

    e, n, pr_key = rsa(25)
    assert 7352796 == decrypt(encrypt(7352796, e, n), pr_key, n)

if __name__ == '__main__':
    main()
