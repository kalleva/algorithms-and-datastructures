#!/usr/bin/env python


def threeway_partition_inplace(l):
    """puts first element in l on its proper place in such a way
    that all elements prior to it is less than him and all after if
    is bigger than him. If there are any elements equal to him,
    they are grouped near him. Algorithm implemented to work in place"""
    i = 0
    j = 0
    r = 0
    while j < len(l) - 1:
        if l[i] > l[i + 1]:
            l[i], l[i + 1] = l[i + 1], l[i]
            i += 1
        elif l[i] == l[i + 1]:
            r += 1
            l.pop(i + 1)
        else:
            l.append(l.pop(i + 1))
        j += 1
    if r > 0:
        while r > 0:
            l.insert(i + 1, l[i])
            r -= 1
    return l


def threeway_partition_not_inplace(l):
    sl = []
    sv = []
    sr = []

    n = l[0]
    for i in l:
        if i < n:
            sl.append(i)
        if i == n:
            sv.append(i)
        if i > n:
            sr.append(i)
    res = []
    res.extend(sl)
    res.extend(sv)
    res.extend(sr)
    return res


def main():
    assert threeway_partition_inplace([1, 1, 1]) == [1, 1, 1]
    assert threeway_partition_inplace([4, 5, 6, 4, 1, 2, 5, 7, 4])[2:5] == [4, 4, 4]


    assert threeway_partition_not_inplace([1, 1, 1]) == [1, 1, 1]
    assert threeway_partition_not_inplace([4, 5, 6, 4, 1, 2, 5, 7, 4])[2:5] == [4, 4, 4]

if __name__ == '__main__':
    main()