#!/usr/bin/env python


def extended_euclid(a, b):
    """integers a and b with a >= b >= 0
    returns: x', y', d such that d = gcd(a, b) and a*x' + b*y' = d"""
    if b == 0:
        return 1, 0, a
    x, y, d = extended_euclid(b, a % b)
    return y, x - int(a / b) * y, d


def main():
    assert extended_euclid(13, 4) == (1, -3, 1)
    assert extended_euclid(25, 11) == (4, -9, 1)
    assert extended_euclid(20, 79) == (4, -1, 1)
    assert extended_euclid(3, 62) == (21, -1, 1)
    assert extended_euclid(21, 91) == (-4, 1, 7)
    assert extended_euclid(5, 23) == (-9, 2, 1)


if __name__ == '__main__':
    main()
