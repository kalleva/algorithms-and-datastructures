#!/usr/bin/env python

import random


def selection(l, k):
    """selects k-less element of array l"""
    assert k < len(l), "can't select k-less element from array which size less than k"
    if len(l) == 1:
        return l[0]
    n = random.randint(0, len(l) - 1)
    sl = []
    sv = []
    sr = []
    for i in l:
        if i < l[n]:
            sl.append(i)
        elif i == l[n]:
            sv.append(i)
        else:
            sr.append(i)
    if k <= len(sl):
        return selection(sl, k)
    elif len(sl) < k <= len(sl) + len(sv):
        return l[n]
    else:
        return selection(sr, k - len(sl) - len(sv))


def main():
    assert selection([2, 36, 5, 21, 8, 13, 11, 20, 5, 4, 1], 8) == 13
    assert selection([1, 2, 3, 4, 5, 6, 7], 4) == 4
    assert selection([1, 1, 1, 1, 1, 2, 1], 6) == 1
    assert selection([1, 1, 1, 1, 1, 2, 1], 6) == 1

if __name__ == '__main__':
    main()