#!/usr/bin/env python

import math


def addition(x, y):
    """School algorithm for addition
       carry:      1
        num1:    1 0 1 1 1     (23)
        num2:  + 1 0 0 1 0 0   (36)
                -------------
        result:  1 1 1 0 1 1   (59)"""
    longer, shorter = ((to_bitarray(x), to_bitarray(y))
                        if x > y
                        else (to_bitarray(y), to_bitarray(x)))

    l = [0] * (len(longer) + 1)
    carry = 0
    temp_sum = 0
    i = 1
    while len(l) - i >= 0:
        if i <= len(shorter):
            temp_sum = longer[-i] + shorter[-i] + carry
        elif i <= len(longer):
            temp_sum = longer[-i] + carry
        else:
            temp_sum = carry

        if temp_sum <= 1:
            l[-i] = temp_sum
        elif temp_sum == 2:
            l[-i] = 0
            carry = 1
        else:
            l[-i] = 1
            carry = 1
        i += 1
    return from_bitarray(l)


def to_bitarray(x):
    """Returns list that is bite representation of x"""
    s = bin(x).lstrip('0b')
    return [int(x) for x in s]


def from_bitarray(x):
    """Converts from bitarray to number"""
    res = 0
    j = 0
    for i in x[::-1]:
        res += i * math.pow(2, j)
        j += 1
    return int(res)


def main():
    assert addition(1, 1) == 2
    assert addition(2, 2) == 4
    assert addition(2, 0) == 2
    assert addition(2, 0) == 2
    assert addition(1010, 1001) == 2011
    assert addition(0, 0) == 0
    assert addition(9, 10000) == 10009
    assert addition(12, 1024) == 1036

if __name__ == '__main__':
    main()
