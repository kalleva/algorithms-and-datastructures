#!/usr/bin/env python


def euclid(a, b):
    """integers a and b with a >= b >= 0
       returns: gcd(x, y)"""
    if b == 0:
        return a
    return euclid(b, a % b)


def main():
    assert euclid(1, 2) == 1
    assert euclid(2, 4) == 2
    assert euclid(378, 546) == 42
    assert euclid(210, 588) == 42


if __name__ == '__main__':
    main()
