#!/usr/bin/env python


def multiply(x, y):
    """school multiplication
       y >= 0
       num1:    1 0 1 1
       num2:  *   1 0 1
               ---------
                1 0 1 1
            + 0 0 0 0
          + 1 0 1 1
          --------------
            1 1 0 1 1 1"""
    bit_y = bin(y).lstrip('0b')
    temp = x
    result = 0
    for i in bit_y[::-1]:
        if i == '1':
            result += temp
        temp <<= 1
    return result


def main():
    assert multiply(-1, 1) == -1
    assert multiply(2, 1) == 2
    assert multiply(2, 2) == 4
    assert multiply(2, 0) == 0
    assert multiply(10, 10) == 100
    assert multiply(0, 0) == 0
    assert multiply(9, 10000) == 90000


if __name__ == '__main__':
    main()
