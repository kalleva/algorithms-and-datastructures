#!/usr/bin/env python

import numpy as np


def matrixfib(n):
    """ [[Fn], [Fn+1] = [[0  1],[1, 1]]^n * [[F0], [F1]]"""
    q = np.array([[0.0, 1.0], [1.0, 1.0]])
    n01 = [[0.0], [1.0]]
    return np.dot(np.linalg.matrix_power(q, n), n01)


def fib(n):
    if n == 0 or n == 1:
        return n
    else:
        return matrixfib(n)[0][0]


def main():
    assert fib(0) == 0
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(9) == 34
    assert fib(50) == 12586269025


if __name__ == '__main__':
    main()
