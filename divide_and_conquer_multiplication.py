# !/usr/bin/env python


def create_mask(start, end):
    """creates mask that consists from 1's starting from start bit
     and ending on end bit"""
    return ((1 << end) - 1) ^ ((1 << start) - 1)


def multiply(x, y):
    """karatsuba algorithm for multiplication"""
    n = max(x.bit_length(), y.bit_length()) // 2
    if n <= 1:
        return x * y
    mask = create_mask(0, n)
    xl, xr = x >> n, x & mask
    yl, yr = y >> n, y & mask

    p1 = multiply(xl, yl)
    p2 = multiply(xr, yr)
    p3 = multiply(xl + xr, yl + yr)
    return (p1 << (n << 1)) + ((p3 - p1 - p2) << n) + p2


def main():
    assert create_mask(0, 1) == 1
    assert create_mask(2, 3) == 4
    assert create_mask(1, 3) == 6
    assert create_mask(0, 3) == 7
    assert create_mask(3, 6) == 56

    assert multiply(1, 2) == 2
    assert multiply(2, 1) == 2
    assert multiply(2, 2) == 4
    assert multiply(4, 4) == 16
    assert multiply(2, 4) == 8
    assert multiply(4, 2) == 8
    assert multiply(8, 8) == 64
    assert multiply(8, 4) == 32
    assert multiply(16, 16) == 256
    assert multiply(3, 3) == 9
    assert multiply(3, 4) == 12
    assert multiply(3, 1) == 3
    assert multiply(6, 3) == 18
    assert multiply(1024, 5) == 5120
    assert multiply(1892347892, 367873) == 696143696073716

if __name__ == '__main__':
    main()