#!/usr/bin/env python


def twoway_partition(l):
    """puts first element in l on its proper place in such a way
    that all elements prior to it is less than him and all after if
    is bigger than him, algorithm implemented to work in place"""
    i = 0
    j = 0
    while j < len(l) - 1:
        if l[i] >= l[i + 1]:
            l[i], l[i + 1] = l[i + 1], l[i]
            i += 1
        else:
            l.append(l.pop(i + 1))
        j += 1
    return l


def main():
    assert twoway_partition([7, 2, 5, 6, 1, 3, 9, 4, 8])[6] == 7
    assert twoway_partition([1, 0, 1, 1, 0, 3])[4] == 1


if __name__ == '__main__':
    main()