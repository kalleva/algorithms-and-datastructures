#!/usr/bin/env python

from modular_exponentiation import mod_pow


def triple_exponentiation(a, b, c, p):
    """returns (a ** (b ** c)) mod p where p is prime.
    uses the fact that if a and p are coprime we can state
    that (a ** (b ** c)) mod p = (a ** ( b ** c mod (p - 1)) mod p)"""
    if b % 2 == 0 and c % 2 == 0 and p == 3:
        return a ** (b ** c) % 3
    return mod_pow(a, mod_pow(b, c, p - 1), p)


def main():
    assert 2 ** (2 ** 2) % 7 == triple_exponentiation(2, 2, 2, 7)
    assert 6 ** (5 ** 2) % 7 == triple_exponentiation(6, 5, 2, 7)
    assert 3 ** (2 ** 15) % 97 == triple_exponentiation(3, 2, 15, 97)
    assert 2 ** (2 ** 2) % 3 == triple_exponentiation(2, 2, 2, 3)
    assert 2 ** (2 ** 4) % 3 == triple_exponentiation(2, 2, 4, 3)
if __name__ == '__main__':
    main()
