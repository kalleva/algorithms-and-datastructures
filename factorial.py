#!/usr/bin/env python


def factorial(a):
    res = 1
    while a > 0:
        res *= a
        a -= 1
    return res


def len_factorial(n):
    count = 0
    while n > 0:
        count += len(bin(n)) - 2
        n -= 1
    return count


def main():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(10) == 3628800
    assert factorial(15) == 1307674368000

    # for i in range(100):
    #     print(str(len(bin(factorial(i))) - 2) + " : " + str(len_factorial(i)))

if __name__ == '__main__':
    main()
