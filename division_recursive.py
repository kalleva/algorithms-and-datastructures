#!/usr/bin/env python


def divide(x, y):
    """recursive division"""
    if x == 0:
        return 0, 0
    q, r = divide(x // 2, y)
    q *= 2
    r *= 2
    if x % 2 == 1:
        r += 1
    if r >= y:
        r -= y
        q += 1
    return q, r


def main():
    assert divide(2, 1) == (2, 0)
    assert divide(1, 2) == (0, 1)
    assert divide(9, 3) == (3, 0)
    assert divide(1024, 512) == (2, 0)
    assert divide(10, 100) == (0, 10)
    assert divide(10, 7) == (1, 3)
    assert divide(12528520681681767, 179426549) == (69825344, 175023911)
    assert divide(12528520681681769, 179426549) == (69825344, 175023913)

if __name__ == '__main__':
    main()
