#!/usr/bin/env python

from random import randint
from modular_exponentiation import mod_pow


def is_prime(n):
    """uses Fermat's test to determine if number is prime.
    Lets through Carmichael numbers and some composites"""
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    a = randint(2, n - 1)
    if mod_pow(a, n - 1, n) == 1:
        return True
    else:
        return False


def main():
    assert is_prime(2)
    assert is_prime(3)
    assert is_prime(17)
    assert is_prime(23)
    assert not is_prime(36)
    assert not is_prime(4)
    assert not is_prime(168)
    assert is_prime(179426549)

if __name__ == '__main__':
    main()
