#!/usr/bin/env python

from collections import deque


def merge_sort_recursive(l):
    if len(l) > 1:
        return merge(merge_sort_recursive(l[:(len(l) // 2)]),
                     merge_sort_recursive(l[(len(l) // 2):]))
    else:
        return l


def merge(x, y):
    res = []
    i = 0
    j = 0
    if len(x) == 0:
        return y
    if len(y) == 0:
        return x
    while i < len(x) and j < len(y):
        if x[i] <= y[j]:
            res.append(x[i])
            i += 1
        else:
            res.append(y[j])
            j += 1
    if i == len(x):
        res.extend(y[j:])
    else:
        res.extend(x[i:])
    return res


def merge_sort_iterative(l):
    if len(l) == 0:
        return l
    queue = deque();
    for i in l:
        queue.append([i])
    while len(queue) > 1:
        queue.append(merge(queue.popleft(), queue.popleft()))
    return queue.pop()


def main():
    assert merge_sort_recursive([]) == []
    assert merge_sort_recursive([1]) == [1]
    assert merge_sort_recursive([2, 1]) == [1, 2]
    assert merge_sort_recursive([1, 3]) == [1, 3]
    assert merge_sort_recursive([2, 1, 3]) == [1, 2, 3]
    assert merge_sort_recursive([5, 4, 2, 3, 1, 6]) == [1, 2, 3, 4, 5, 6]
    assert (merge_sort_recursive([1, 2, 2, 3, 2, 3, 4, 1, 4]) ==
            [1, 1, 2, 2, 2, 3, 3, 4, 4])
    assert (merge_sort_recursive([1, 10, 5, 6, 20, 100, 99]) ==
            [1, 5, 6, 10, 20, 99, 100])

    assert merge_sort_iterative([]) == []
    assert merge_sort_iterative([1]) == [1]
    assert merge_sort_iterative([2, 1]) == [1, 2]
    assert merge_sort_iterative([1, 3]) == [1, 3]
    assert merge_sort_iterative([2, 1, 3]) == [1, 2, 3]
    assert merge_sort_iterative([5, 4, 2, 3, 1, 6]) == [1, 2, 3, 4, 5, 6]
    assert (merge_sort_iterative([1, 2, 2, 3, 2, 3, 4, 1, 4]) ==
            [1, 1, 2, 2, 2, 3, 3, 4, 4])
    assert (merge_sort_iterative([1, 10, 5, 6, 20, 100, 99]) ==
            [1, 5, 6, 10, 20, 99, 100])


if __name__ == '__main__':
    main()