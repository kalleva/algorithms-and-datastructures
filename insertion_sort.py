#!/usr/bin/env python


def insertion_sort(l):
    if len(l) <= 1:
        return l
    i = 1
    while i < len(l):
        j = i
        while l[j] < l[j - 1] and j > 0:
            l[j - 1], l[j] = l[j], l[j - 1]
            j -= 1
        i += 1
    return l


def main():
    assert insertion_sort([]) == []
    assert insertion_sort([1]) == [1]
    assert insertion_sort([2, 1]) == [1, 2]
    assert insertion_sort([1, 3]) == [1, 3]
    assert insertion_sort([2, 1, 3]) == [1, 2, 3]
    assert insertion_sort([5, 4, 2, 3, 1, 6]) == [1, 2, 3, 4, 5, 6]
    assert (insertion_sort([1, 2, 2, 3, 2, 3, 4, 1, 4]) ==
            [1, 1, 2, 2, 2, 3, 3, 4, 4])
    assert (insertion_sort([1, 10, 5, 6, 20, 100, 99]) ==
            [1, 5, 6, 10, 20, 99, 100])


if __name__ == '__main__':
    main()
