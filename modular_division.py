#!/usr/bin/env python

from modular_multiplication import mod_multiply
from extended_euclid import extended_euclid

"""a has multiplicative inverse modulo N if and only
if it is relatively prime to N.
a * -a mod N"""


def mod_divide(a, b, N):
    x, y, d = extended_euclid(b, N)
    if d != 1:
        return
    inverse_b = x % N
    return mod_multiply(a, inverse_b, N)


def main():
    assert mod_divide(6, 5, 7) == 4
    assert mod_divide(2, 3, 7) == 3
    assert mod_divide(3, 3, 7) == 1
    assert mod_divide(5, 3, 7) == 4
    assert mod_divide(2, 11, 25) == 7


if __name__ == '__main__':
    main()
