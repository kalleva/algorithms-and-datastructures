#!/usr/bin/env python


def multiply(x, y):
    """recursive multiplication algorithm
       y >= 0"""
    if y == 0:
        return 0
    temp = multiply(x, int(y / 2))
    if y % 2 == 0:
        return 2 * temp
    else:
        return x + 2 * temp


def main():
    assert multiply(1, 1) == 1
    assert multiply(-1, 1) == -1
    assert multiply(2, 1) == 2
    assert multiply(2, 2) == 4
    assert multiply(2, 0) == 0
    assert multiply(10, 10) == 100
    assert multiply(0, 0) == 0
    assert multiply(2, 9) == 18
    assert multiply(9, 10000) == 90000


if __name__ == '__main__':
    main()
