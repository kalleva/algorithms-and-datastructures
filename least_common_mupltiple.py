#!/usr/bin/env python

from euclid import euclid


def lcm(a, b):
    """returns least common multiple of a and b"""
    return a * b / euclid(a, b)


def main():
    assert lcm(3, 8) == 24
    assert lcm(8, 8) == 8
    assert lcm(1, 1) == 1
    assert lcm(7, 8) == 56
    assert lcm(8, 7) == 56
    assert lcm(6, 4) == 12
    assert lcm(4, 6) == 12
    assert lcm(133, 21) == 399
    assert lcm(478645, 4325) == 414027925

if __name__ == '__main__':
    main()
