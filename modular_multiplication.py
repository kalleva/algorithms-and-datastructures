#!/usr/bin/env python

from division_recursive import divide


def mod_multiply(x, y, N):
    x_mod_N = x % N
    y_mod_N = y % N
    result = x_mod_N * y_mod_N
    return result if result < N else divide(result, N)[1]


def main():
    assert mod_multiply(4, 7, 3) == 1
    assert mod_multiply(169, 6, 13) == 0
    assert mod_multiply(0, 6, 13) == 0
    assert mod_multiply(2, 0, 13) == 0
    assert mod_multiply(-1, 6, 13) == 7
    assert mod_multiply(2, 5, 7) == 3
    assert mod_multiply(92877999, 149743537 * 149743537, 179426549) == 92877999 * 149743537 * 149743537 % 179426549


if __name__ == '__main__':
    main()
