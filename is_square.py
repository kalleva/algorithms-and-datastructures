#!/usr/bin/env python


def is_square(n):
    i = 1
    while True:
        if n < 0:
            return False
        if n == 0:
            return True
        n -= i
        i += 2


def main():
    assert is_square(1)
    assert is_square(4)
    assert not is_square(3)
    assert is_square(16)
    assert is_square(25)
    assert is_square(1024)
    assert is_square(152942689)
    assert is_square(152942689)
    assert is_square(15903304850027990366676475401990043876)

if __name__ == '__main__':
    main()
