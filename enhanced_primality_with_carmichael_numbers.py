#!/usr/bin/env python

from modular_exponentiation import mod_pow
from euclid import euclid
import math
import random


def is_prime(n):
    """uses Fermat's test to determine if number is prime.
    Stops most of the Carmichael numbers with help of Rabin
    and Miller algorithm"""

    if n == 2:
        return True
    if n % 2 == 0:
        return False
    a = [random.randint(2, n - 1) for i in range(100)]
    for item in a:
        if mod_pow(item, n - 1, n) != 1:
            return False

    return rabin_miller_test(n)
    return True


def distinguish_powers_of_two(n):
    """returns representation of n = 2 ^ pow_of_two * u"""
    pow_of_two = 0
    while n % 2 == 0:
        n /= 2
        pow_of_two += 1
    return int(n), pow_of_two


def rabin_miller_test(n):
    """returns True if prime, False otherwise"""
    list_of_first_primes = [i for i in range(200) if simple_primality_test(i)]
    for item in list_of_first_primes:
        if n > 560 and euclid(item, n) == 1:
            t, u = distinguish_powers_of_two(n - 1)
            l = [mod_pow(item, t * math.pow(2, i), n) for i in range(u + 1)]
            if l[0] % n != 1:
                try:
                    if not (l[l.index(1) - 1] == -1 % n):
                        return False
                except ValueError:
                    continue
    return True


def simple_primality_test(n):
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


def main():
    assert is_prime(2)
    assert is_prime(3)
    assert is_prime(17)
    assert is_prime(23)
    assert not is_prime(36)
    assert not is_prime(4)
    assert not is_prime(168)

    # below are tests for Carmichael numbers
    assert not is_prime(561)
    assert not is_prime(1105)
    assert not is_prime(1729)
    assert not is_prime(2465)
    assert not is_prime(2821)
    assert not is_prime(6601)
    assert not is_prime(8911)
    assert not is_prime(10585)
    assert not is_prime(15841)
    assert not is_prime(29341)
    assert not is_prime(314821)
    assert not is_prime(334153)
    assert not is_prime(340561)
    assert not is_prime(399001)
    assert not is_prime(399001)
    assert not is_prime(449065)


if __name__ == '__main__':
    main()
