#!/usr/bin/env python


def mod_add(x, y, N):
    x_mod_N = x % N
    y_mod_N = y % N
    result = x_mod_N + y_mod_N
    return result if result < N else result - N


def main():
    assert mod_add(5, 4, 3) == 0
    assert mod_add(169, 6, 13) == 6


if __name__ == '__main__':
    main()
