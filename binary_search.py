#!/usr/bin/env python


def binary_search(i, l, zero=0):
    """searches for index of i in sorted array l
    if i not in l returns -1"""
    res = -1
    if len(l) == 0:
        return res
    half = len(l) // 2
    if i == l[half]:
        return zero + half
    elif i < l[half]:
        res = binary_search(i, l[:half], zero)
    else:
        res = binary_search(i, l[half + 1:], zero + half + 1)
    return res


def array_search(s, l):
    return [x + 1 if x != -1 else x for x in [binary_search(x, s) for x in l]]


def main():
    assert binary_search(0, [1, 2, 3, 4, 5, 6]) == -1
    assert binary_search(1, [1, 2, 3, 4, 5, 6]) == 0
    assert binary_search(2, [1, 2, 3, 4, 5, 6]) == 1
    assert binary_search(3, [1, 2, 3, 4, 5, 6]) == 2
    assert binary_search(4, [1, 2, 3, 4, 5, 6]) == 3
    assert binary_search(5, [1, 2, 3, 4, 5, 6]) == 4
    assert binary_search(6, [1, 2, 3, 4, 5, 6]) == 5
    assert binary_search(12, [1, 2, 4, 5, 6, 9, 11, 12, 234, 3646, 4576556, 5436447567]) == 7

    assert array_search([10, 20, 30, 40, 50], [40, 10, 35, 15, 40, 20]) == [4, 1, -1, -1, 4, 2]


if __name__ == '__main__':
    main()