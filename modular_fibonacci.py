#!/usr/bin/env python

from modular_addition import mod_add


def mod_fib(n, p):
    """computes Fib(n) mod p"""
    if n == 0 or n == 1:
        return n
    x = 0
    y = 1
    while n > 0:
        y, x = mod_add(x, y, p), y % p
        n -= 1
    return x


def main():
    assert mod_fib(1, 2) == 1
    assert mod_fib(3, 4) == 2
    assert mod_fib(4, 2) == 1
    assert mod_fib(9, 31) == 3
    assert mod_fib(14, 16) == 9
    assert mod_fib(29, 49) == 23
    assert mod_fib(98, 7697) == 4914


if __name__ == '__main__':
    main()
