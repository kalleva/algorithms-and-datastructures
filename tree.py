#!/usr/bin/env python


class Tree:

    # to create node of Tree call Tree(entry, [])
    # if you don't give __init__ empty list it will use default for parameter children
    # but this default list will be shared between all instances of Tree created this way
    # so if this is not desired behaviour use empty list
    def __init__(self, entry, children=[]):
        self.entry = entry
        for x in children:
            assert isinstance(x, Tree)
        self.children = children

    def is_leaf(self):
        return not self.children


def rand_tree(n, k):
    """generates random k-ary tree with n nodes"""
    from random import randint
    num = n
    root = Tree(n - num + 1, [])
    l = [root]
    num -= 1

    while num > 0:
        added = False
        while not added:
            i = randint(0, len(l) - 1)
            if len(l[i].children) < k:
                node = Tree(n - num + 1, [])
                l[i].children.append(node)
                l.append(node)
                added = True
                num -= 1
    return root


def max_depth(root):
    """finds maximum depth of tree"""
    x = []
    y = []
    x.append(root)
    depth = 0

    while len(x) > 0 or len(y) > 0:
        if len(x) > 0:
            while x:
                y.extend(x.pop().children)
            depth += 1
            continue
        if len(y) > 0:
            while y:
                x.extend(y.pop().children)
            depth += 1
            continue
    return depth


def main():
    a = Tree(1)
    assert max_depth(a) == 1

    a = Tree(1, [Tree(2, [Tree(3, [])])])
    assert max_depth(a) == 3

    a = Tree(1, [Tree(2, [Tree(4, [Tree(5, [])])]), Tree(3, [Tree(1, [])])])
    assert max_depth(a) == 4

    a = Tree(1, [Tree(2,
                      [Tree(5,
                            [Tree(6, []),
                             Tree(7,
                                  [Tree(8, [])])])]),
                 Tree(3, []),
                 Tree(4, [Tree(9,
                               [Tree(10, []),
                                Tree(11,
                                     [Tree(12,
                                           [(Tree(13,
                                                  [Tree(14, [])]))])])])])])
    assert max_depth(a) == 7

if __name__ == '__main__':
    main()
