#!/usr/bin/env python

from modular_multiplication import mod_multiply


def mod_pow(x, y, N):
    if y % N == 0:
        return 1
    temp = mod_pow(x, int(y / 2), N)
    if y % 2 == 0:
        return mod_multiply(temp, temp, N)
    else:
        return mod_multiply(x, mod_multiply(temp, temp, N), N)


def main():
    assert mod_pow(2, 4, 6) == 4
    assert mod_pow(8, 3, 7) == 1
    assert mod_pow(1024, 10, 3) == 1
    assert mod_pow(4, 13, 497) == 445
    assert mod_pow(92877999, 179426548, 179426549) == 1
    assert mod_pow(2, 125, 127) == 64

if __name__ == '__main__':
    main()
