#!/usr/bin/env python

import random


def is_divisible(num, r, n):
    """divides num into tuples of size r starting from the right end
    and returns if sum of those tuples are divisible by n"""
    s = 0
    while num > 0:
        s += num - (num // 10 ** r) * 10 ** r
        num //= 10 ** r
    if s % n == 0:
        return True
    return False


def divisibility_rule(n):
    """returns size of the tuple for divisibility test"""
    r = 0
    found = False
    while not found:
        r += 1
        found = True
        for k in range(1, 15):
            s = 0
            for i in [2, 3, 5, 7, 11, 13]:
                s += i * (10 ** k)
                found = found and (is_divisible(s, r, n) == (s % n == 0) and is_divisible(s * n, r, n))
    return r


def main():
    assert divisibility_rule(3) == 1
    assert divisibility_rule(11) == 2
    assert divisibility_rule(37) == 3

    print(divisibility_rule(13))
    print(divisibility_rule(17))

if __name__ == '__main__':
    main()
