#!/usr/bin/env python

import math


def is_power(n):
    """determines if n = q ^ k, where q, k integers > 1"""
    pw = 2
    while pw <= math.floor(math.log(n, 2)):
        a = 1
        b = n
        while b - a > 1:
            m = (a + b) // 2
            res = m ** pw
            if res == n:
                return True
            elif res > n:
                b = m
            else:
                a = m
        pw += 1
    return False


def main():
    assert is_power(4)
    assert not is_power(2)
    assert not is_power(3)
    assert not is_power(10)
    assert is_power(9)
    assert is_power(27)
    assert is_power(169)
    assert not is_power(168)
    assert is_power(8303765625)
    assert is_power(75918020127607)
    assert not is_power(75918020127608)

if __name__ == '__main__':
    main()
