#!/usr/bin/env python


def count_inverses(p, n):
    """for prime p counts how many elements of {0, 1, ... , p ** n - 1}
    have inverse modulo p ** n"""
    count = 0
    for i in range(1, p ** n):
        if i % p != 0:
            count += 1
    return count


def count_inverses_with_euclid(p, n):
    """same that count inverses but uses euclid algorithm for determining gcd"""
    from euclid import euclid
    count = 0
    for i in range(1, p ** n):
        if euclid(i, p) == 1:
            count += 1
    return count


def main():
    assert count_inverses(11, 3) == 1210
    assert count_inverses(11, 3) == count_inverses_with_euclid(11, 3)
    assert count_inverses(17, 4) == count_inverses_with_euclid(17, 4)

    # general formula for number of inverses for modulo p ** n: p ** n - p ** (n - 1)
    assert count_inverses(17, 4) == 17 ** 4 - 17 ** 3
    assert count_inverses(11, 3) == 11 ** 3 - 11 ** 2
    assert count_inverses(2, 15) == 2 ** 15 - 2 ** 14
    assert count_inverses(13, 1) == 13 ** 1 - 13 ** 0

if __name__ == '__main__':
    main()
